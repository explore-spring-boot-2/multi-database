package id.lab.config;

import java.util.HashMap;

import javax.sql.DataSource;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@EnableJpaRepositories(basePackages = "id.lab.dao.hsql", entityManagerFactoryRef = "hsqlEntityManager", transactionManagerRef = "hsqlTransactionManager")
@PropertySource("classpath:./persistence-hsql.properties")
public class HsqlConfig {
	
	@Autowired Environment env;

    @Bean
    public DataSource hsqlDs() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(env.getProperty("datasource.driverClassName"));
        dataSourceBuilder.url(env.getProperty("datasource.url"));
        dataSourceBuilder.username(env.getProperty("datasource.username"));
        dataSourceBuilder.password(env.getProperty("datasource.password"));
        return dataSourceBuilder.build();
    }
    
    @Bean
    public LocalContainerEntityManagerFactoryBean hsqlEntityManager() {
        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(hsqlDs());
        em.setPackagesToScan("id.lab.entity.hsql");
        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        final HashMap<String, Object> properties = new HashMap<String, Object>();
        // properties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
        // properties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");
        em.setJpaPropertyMap(properties);

        return em;
    }

	@Bean
	public PlatformTransactionManager hsqlTransactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(hsqlEntityManager().getObject());
		return transactionManager;
	}	
}
