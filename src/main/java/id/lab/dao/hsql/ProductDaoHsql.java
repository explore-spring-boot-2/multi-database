package id.lab.dao.hsql;

import org.springframework.data.repository.PagingAndSortingRepository;

import id.lab.entity.hsql.Product;

public interface ProductDaoHsql extends PagingAndSortingRepository<Product, Long>{

}
