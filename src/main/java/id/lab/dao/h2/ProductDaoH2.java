package id.lab.dao.h2;

import org.springframework.data.repository.PagingAndSortingRepository;

import id.lab.entity.h2.Product;

public interface ProductDaoH2 extends PagingAndSortingRepository<Product, Long>{

}
