package id.lab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiDabaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultiDabaseApplication.class, args);
	}

}
