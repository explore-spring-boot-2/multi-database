package id.lab.entity.h2;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity @Data
public class Product {
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	private long id;
	
	@Column(length = 15)
	private String code;
	
	@Column(length = 50)
	private String name;
}
