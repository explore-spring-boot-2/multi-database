package id.lab.hsql;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlConfig;

import id.lab.entity.hsql.Product;
import id.lab.dao.hsql.ProductDaoHsql;

@Sql(scripts="/sqlscripts/init-hsql.sql", config = @SqlConfig(dataSource = "hsqlDs", transactionManager = "hsqlTransactionManager"), executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
@SpringBootTest
public class ProductTests {
	@Autowired private ProductDaoHsql productDao;
	@Test
	void createProduct() {
		Product product = new Product();
		product.setCode("hsql");
		product.setName("Hyper SQL Database");
		productDao.save(product);
		System.out.println("===== Hyper SQL =====");
		productDao.findAll().forEach(it -> {
			System.out.println(it.getId() + " | " + it.getName());
		});
		System.out.println("===== END OF Hyper SQL =====");
	}
}
