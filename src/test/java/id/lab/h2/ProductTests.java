package id.lab.h2;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlConfig;

import id.lab.entity.h2.Product;
import id.lab.dao.h2.ProductDaoH2;

@Sql(scripts="/sqlscripts/init-h2.sql", config = @SqlConfig(dataSource = "h2Ds", transactionManager = "h2TransactionManager"), executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
@SpringBootTest
public class ProductTests {
	@Autowired private ProductDaoH2 productDao;
	@Test
	void createProduct() {
		Product product = new Product();
		product.setCode("h2db");
		product.setName("H2 Database");
		productDao.save(product);
		System.out.println("===== H2 =====");
		productDao.findAll().forEach(it -> {
			System.out.println(it.getId() + " | " + it.getName());
		});
		System.out.println("===== END OF H2 =====");
	}
}
